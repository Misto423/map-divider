﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CastleGame.Input
{
    public class InputHelper
    {
        #region StateVars
        private GamePadState _lastGamepadState;
        private GamePadState _currentGamepadState;
#if (!XBOX)
        private KeyboardState _lastKeyboardState;
        private KeyboardState _currentKeyboardState;
#endif
        private PlayerIndex _index = PlayerIndex.One;
        private bool refreshData = false;
        #endregion

        public void Update() //Fetches the latest input states.
        {
            if (!refreshData)
                refreshData = true;
            if (_lastGamepadState == null && _currentGamepadState == null)
            {
                _lastGamepadState = _currentGamepadState = GamePad.GetState(_index);
            }
            else
            {
                _lastGamepadState = _currentGamepadState;
                _currentGamepadState = GamePad.GetState(_index);
            }
#if (!XBOX)
            if (_lastKeyboardState == null && _currentKeyboardState == null)
            {
                _lastKeyboardState = _currentKeyboardState = Keyboard.GetState();
            }
            else
            {
                _lastKeyboardState = _currentKeyboardState; _currentKeyboardState = Keyboard.GetState();
            }
#endif
        }

        #region GetStates
        public GamePadState LastGamepadState
        {
            get { return _lastGamepadState; }
        }
        public GamePadState CurrentGamepadState
        {
            get { return _currentGamepadState; }
        }

        //the index that is used to poll the gamepad.
        public PlayerIndex Index
        {
            get { return _index; }
            set
            {
                _index = value;
                if (refreshData)
                {
                    Update();
                    Update();
                }
            }
        }

#if (!XBOX)
        public KeyboardState LastKeyboardState
        {
            get { return _lastKeyboardState; }
        }
        public KeyboardState CurrentKeyboardState
        {
            get { return _currentKeyboardState; }
        }
#endif
        #endregion

        #region AnalogStick
        public Vector2 LeftStickPosition
        {
            get
            {
                return new Vector2(
                    _currentGamepadState.ThumbSticks.Left.X,
                    -CurrentGamepadState.ThumbSticks.Left.Y);
            }
        }

        public Vector2 LeftStickVelocity
        {
            get
            {
                Vector2 temp =
                    _currentGamepadState.ThumbSticks.Left -
                    _lastGamepadState.ThumbSticks.Left;
                return new Vector2(temp.X, -temp.Y);
            }
        }
        #endregion

        #region Triggers
        public float LeftTriggerPosition
        {
            get { return _currentGamepadState.Triggers.Left; }
        }

        public float RightTriggerPosition
        {
            get { return _currentGamepadState.Triggers.Right; }
        }

        public float LeftTriggerVelocity
        {
            get
            {
                return _currentGamepadState.Triggers.Left - _lastGamepadState.Triggers.Left;
            }
        }

        public float RightTriggerVelocity
        {
            get
            {
                return _currentGamepadState.Triggers.Right - _lastGamepadState.Triggers.Right;
            }
        }
        #endregion

        #region New/Current/Old Press Checker
        //selected button is being pressed in the current state AND NOT the last state.
        public bool IsNewPress(Buttons button)
        {
            return (_lastGamepadState.IsButtonUp(button) && _currentGamepadState.IsButtonDown(button));
        }

        //selected button is being pressed in the current state AND in the last state.
        public bool IsCurPress(Buttons button)
        {
            return (_lastGamepadState.IsButtonDown(button) && _currentGamepadState.IsButtonDown(button));
        }

        //selected button is NOT being pressed in the current state AND is being pressed in the last state.
        public bool IsOldPress(Buttons button)
        {
            return (_lastGamepadState.IsButtonDown(button) && _currentGamepadState.IsButtonUp(button));
        }

#if (!XBOX360)
        //selected key is being pressed in the current state AND NOT in the last state.
        public bool IsNewPress(Keys key)
        {
            return (_lastKeyboardState.IsKeyUp(key) && _currentKeyboardState.IsKeyDown(key));
        }

        //selected key is being pressed in the current state AND in the last state.
        public bool IsCurPress(Keys key)
        {
            return (_lastKeyboardState.IsKeyDown(key) && _currentKeyboardState.IsKeyDown(key));
        }
        //selected key is NOT being pressed in the current state AND being pressed in the last state.
        public bool IsOldPress(Keys key)
        {
            return (_lastKeyboardState.IsKeyDown(key) && _currentKeyboardState.IsKeyUp(key));
        }
#endif
        #endregion
    }
}
