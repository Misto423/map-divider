﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CastleGame.GameLogic
{
    public class MapSelection
    {
        private int mapIndex = 0;
        Graphics.Map currentMap;
        private Graphics.Map[] mapList;

        public MapSelection(Graphics.Map[] mapList)
        {
            this.mapList = mapList;
        }

        public Graphics.Map CurrentMap
        {
            get { return currentMap; }
            set { currentMap = value; }
        }

        public void updateMapSelection(GraphicsDevice gd, SpriteBatch sb, GameTime gt, Input.InputHelper inputData)
        {
            
            inputData.Update();
            if (inputData.IsNewPress(Buttons.LeftThumbstickLeft))
            {
                if (mapIndex == 0)
                {
                    mapIndex = mapList.GetLength(0) - 1;
                }
                else
                {
                    mapIndex--;
                }

            }
            if (inputData.IsNewPress(Buttons.LeftThumbstickRight))
            {
                if (mapIndex == mapList.GetLength(0) - 1)
                {
                    mapIndex = 0;
                }
                else
                {
                    mapIndex++;
                }
            }
            if (inputData.IsNewPress(Buttons.A) || inputData.IsNewPress(Buttons.Start))
            {
                currentMap = mapList[mapIndex];
                Graphics.MapDecoder.generateIndividualMaps(gd, sb, ref currentMap);
                GameLibrary.currentGameState = GameState.BuyPhase;
            }
#if (!XBOX)
            if (inputData.IsNewPress(Keys.A) || inputData.IsNewPress(Keys.Left))
            {
                if (mapIndex == 0)
                {
                    mapIndex = mapList.GetLength(0) - 1;
                }
                else
                {
                    mapIndex--;
                }
            }
            if (inputData.IsNewPress(Keys.D) || inputData.IsNewPress(Keys.Right))
            {
                if (mapIndex == mapList.GetLength(0) - 1)
                {
                    mapIndex = 0;
                }
                else
                {
                    mapIndex++;
                }
            }
            if (inputData.IsNewPress(Keys.Enter))
            {
                currentMap = mapList[mapIndex];
                Graphics.MapDecoder.generateIndividualMaps(gd, sb, ref currentMap);
                GameLibrary.currentGameState = GameState.BuyPhase;
            }
#endif

            //inputData.Update();
        }


        public void drawMapSelection(SpriteBatch sb)
        {
            sb.Begin();
            //draw simple data
            sb.DrawString(GameLibrary.GameFont, mapList[mapIndex].Title, new Vector2(10, 10), Color.Green);
            sb.DrawString(GameLibrary.GameFont, "Num Castles: " + mapList[mapIndex].NumberOfCastles, new Vector2(10, 25), Color.Green);
            sb.DrawString(GameLibrary.GameFont, "Num Players: " + mapList[mapIndex].NumberOfPlayers, new Vector2(10, 40), Color.Green);
            sb.DrawString(GameLibrary.GameFont, mapList[mapIndex].MapDimensionString, new Vector2(10, 55), Color.Green);
            sb.DrawString(GameLibrary.GameFont, "Tileset: " + mapList[mapIndex].TilesetName, new Vector2(10, 70), Color.Green);
            sb.Draw(mapList[mapIndex].MapImage, new Vector2(10, 100), null, Color.White, 0.0f, new Vector2(0, 0), (float)1 / (GameLibrary.TILESIZE / 8), SpriteEffects.None, 0.0f);
            sb.End();

        }
    }
}
