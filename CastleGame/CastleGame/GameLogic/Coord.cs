﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CastleGame.GameLogic
{
    public class Coord
    {
        private int vert, horz;

        public Coord()
        {
            vert = 0;
            horz = 0;
        }

        public Coord(int vert, int horz)
        {
            this.vert = vert;
            this.horz = horz;
        }

        public int Vertical
        {
            get {return vert;}
            set { vert = value; }
        }

        public int Horizontal
        {
            get { return horz; }
            set { horz = value; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            Coord c = obj as Coord;
            if ((System.Object)c == null) return false;

            return (this.Vertical == c.Vertical) && (this.Horizontal == c.Horizontal);
        }

        public bool Equals(Coord c)
        {
            if ((Object)c == null) return false;

            return (this.Vertical == c.Vertical) && (this.Horizontal == c.Horizontal);
        }

        public override int GetHashCode()
        {
            return this.Vertical ^ this.Horizontal;
        }

        public override string ToString()
        {
            return this.Vertical + " , " + this.Horizontal;
        }
    }
}
