﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CastleGame.GameLogic
{
    public enum GameState
    {
        Menu,
        GameStart,
        AttackPhase,
        BuyPhase,
        RebuildPhase
    }

    public enum MenuState
    {
        Title,
        Lobby,
        MapSelect,
        Stats,
        Leaderboards,
        GameOver
    }

    class GameLibrary
    {
        //game states
        public static GameState currentGameState = GameState.Menu;
        public static MenuState currentMenuState = MenuState.MapSelect;
        
        //control arrays
        public static Keys[] keyboardControls;
        public static Buttons[] gamepadControls;

        //global graphics stuff
        public static SpriteFont GameFont;
        public static readonly int TILESIZE = 32;
    }
}
