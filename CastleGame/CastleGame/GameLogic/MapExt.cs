﻿using System;
using System.IO;
using System.Linq;
using Associations;

namespace CastleGame.GameLogic
{
    public class MapExt
    {
        public static string extension = ".qxm";
        public static void handleMapExt()
        {
            AF_FileAssociator fileAssoc = new AF_FileAssociator(extension);
            //create a file association for the map builder 
            fileAssoc.Create("CastleGame", "Castle Game Map File", null, 
                new ExecApplication(@"C:\downloads\CGMapEditor.exe"), new OpenWithList(new string[] { "Castle Game Map Editor" }));

            fileAssoc.Delete();
        }
    }
}
