﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CastleGame.Graphics
{
    public class Map
    {
        #region Private Vars
        private string title;
        private string tilesetName;
        private byte numPlayers;
        private byte numCastles;
        private string mapDim;
        private int mapWidth;
        private int mapHeight;
        private int[,] mapArray;
        private List<byte[,]> playerStartingArrays;
        private GameLogic.Coord[] castleLoc;
        private Texture2D mapImage;
        private Texture2D tileset;
        private Texture2D[] playerMaps;
        private Texture2D mapThumb;
        #endregion

        #region Attributes

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public string TilesetName
        {
            get { return tilesetName; }
            set { tilesetName = value; }
        }

        public byte NumberOfPlayers
        {
            get { return numPlayers; }
            set { numPlayers = value; }
        }

        public byte NumberOfCastles
        {
            get { return numCastles; }
            set { numCastles = value; }
        }
        public string MapDimensionString
        {
            get { return mapDim; }
            set { mapDim = value; }
        }
        public int MapWidth
        {
            get { return mapWidth; }
            set { mapWidth = value; }
        }
        public int MapHeight
        {
            get { return mapHeight; }
            set { mapHeight = value; }
        }
        public int[,] MapArray
        {
            get { return mapArray; }
            set { mapArray = value; }
        }
        public List<byte[,]> PlayerStartingArrays
        {
            get { return playerStartingArrays; }
            set { playerStartingArrays = value; }
        }
        public GameLogic.Coord[] CastleLocations
        {
            get { return castleLoc; }
            set { castleLoc = value; }
        }
        public Texture2D MapImage
        {
            get { return mapImage; }
            set { mapImage = value; }
        }
        public Texture2D TilesetImage
        {
            get { return tileset; }
            set { tileset = value; }
        }
        public Texture2D[] PlayerMaps
        {
            get { return playerMaps; }
            set { playerMaps = value; }
        }
        public Texture2D MapThumb
        {
            get { return mapThumb; }
            set { mapThumb = value; }
        }

        #endregion
    }
}
