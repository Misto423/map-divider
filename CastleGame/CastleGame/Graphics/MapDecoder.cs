﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using CastleGame.GameLogic;

namespace CastleGame.Graphics
{
    public static class MapDecoder
    {
        //list of all the .qxm map files
        public static List<FileInfo> mapFiles = new List<FileInfo>();

        //reads in the maps during map selection
        public static void getMapsInDirectory(string rootDir)
        {
            //get the directory of map files
            DirectoryInfo mapDir = new DirectoryInfo(rootDir + @"\Maps");
            try
            {
                //search through the directory and add in all .qxm files
                foreach (FileInfo mapFile in mapDir.GetFiles("*" + GameLogic.MapExt.extension, SearchOption.TopDirectoryOnly))
                {
                    mapFiles.Add(mapFile);
                }
            }
            catch //return if there is an error finding directory
            {
                Console.WriteLine(mapDir.FullName + " could not be accessed!");
                return;
            }
        }

        //displays the map thumbnail for selection screen
        //probably not needed since I can scale the overall map down
        //could be edited later to use an image other then scaled down image
        //public static Texture2D showMapThumb(ContentManager cm, string mapName, string rootDir)
        //{
        //    //get the directory of map files
        //    DirectoryInfo mapDir = new DirectoryInfo(rootDir + @"\Maps");
        //    //get a list of image files in the map directory
        //    List<FileInfo> thumbFiles = mapDir.GetFiles("*.xnb", SearchOption.TopDirectoryOnly).ToList<FileInfo>();
        //    //if the image name matches the map name, use it as a thumbnail
        //    FileInfo f = thumbFiles.Find(
        //        delegate(FileInfo file)
        //        {
        //            return (file.Name == mapName);
        //        });

        //    //if the thumb was found, load it
        //    if (f.Exists)
        //    {
        //        //load image and return it to display
        //        Texture2D thumb = cm.Load<Texture2D>(mapName);

        //        return thumb;
        //    }
        //    //if the image doesn't exist, return null texture
        //    //could replace with a texture for "thumb not found"
        //    return null;
        //}

        //decodes the map file and generates a texture and array of the actual map
        //also gets all the map information in the file, such as map name, tileset used
        //number of players, etc.
        //once a map is selected, the info gathered and genrated from this function will be used
        //by the generateIndividualMaps function to create the sub maps

        public static Map generateMap(string mapName, string contentRoot, ContentManager cm, GraphicsDevice gd, SpriteBatch sb)
        {
            string mapInterp = "";
            Map createdMap = new Map();
            //check that the file still exists (it should)
            if (File.Exists(mapName))
            {
                //open and read data from the map file
                using (FileStream fs = File.OpenRead(mapName))
                {
                    byte[] b = new byte[1024];
                    UTF8Encoding temp = new UTF8Encoding(true);
                    //get data out of the file and store in the byte array
                    while (fs.Read(b, 0, b.Length) > 0)
                    {
                        //convert the byte array to a string to interpret
                        mapInterp = temp.GetString(b);
                    }
                }

                //interpret map data
                #region Map Interpretation (Basic Data)
                //assign title of map
                createdMap.Title = interpData(ref mapInterp);
                //assign tileset name for the map to pull tiles from
                createdMap.TilesetName = interpData(ref mapInterp);
                //assign number of players
                createdMap.NumberOfPlayers = Convert.ToByte(interpData(ref mapInterp));
                //assign number of castles
                createdMap.NumberOfCastles = Convert.ToByte(interpData(ref mapInterp));
                //assign map dimension string
                createdMap.MapDimensionString = interpData(ref mapInterp);
                //get map width amd height from map dimension string
                int xindex = createdMap.MapDimensionString.IndexOf('x');
                createdMap.MapWidth = Convert.ToInt32(createdMap.MapDimensionString.Substring(0, xindex)) * GameLibrary.TILESIZE;
                createdMap.MapHeight = Convert.ToInt32(createdMap.MapDimensionString.Substring(xindex + 1,
                    createdMap.MapDimensionString.Length - (xindex + 1))) * GameLibrary.TILESIZE;
                //adjust map dimension string
                createdMap.MapDimensionString = createdMap.MapWidth + " x " + createdMap.MapHeight;
                //remove remaining /r/n
                mapInterp = mapInterp.Substring(2, mapInterp.Length - 2);
                #endregion

                //load in the tileset
                #region TileSet Loader Code
                DirectoryInfo mapDir = new DirectoryInfo(contentRoot + @"\Maps\TileSets");

                try
                {
                    //search through the directory for the correct tile set
                    foreach (FileInfo tileSetFile in mapDir.GetFiles(createdMap.TilesetName + ".xnb", SearchOption.TopDirectoryOnly))
                    {
                        createdMap.TilesetImage = cm.Load<Texture2D>(@"Maps\TileSets\" + tileSetFile.Name.Substring(0, tileSetFile.Name.IndexOf('.')));
                    }
                }
                catch //return if there is an error finding directory
                {
                    Console.WriteLine(mapDir.FullName + " could not be accessed!");
                    return null;
                }
                #endregion


                #region Create Map Array
                //create arrays for map and castle data
                createdMap.MapArray = new int[(createdMap.MapHeight / GameLibrary.TILESIZE), (createdMap.MapWidth / GameLibrary.TILESIZE)];
                Coord[] castleList = new Coord[createdMap.NumberOfCastles+1];

                //convert the remaining data in the map file to a map texture
                //array indices
                int vert = 0, horz = 0;
                int cindex = 0;
                while (true)
                {
                    int tile = readCharacterAsInt(ref mapInterp);
                    //exit loop if all map data is read
                    if (tile == -2)
                    {
                        break;
                    }
                    //if a castle is found, add its location to the array
                    if (tile == 0)
                    {
                        castleList[cindex] = new Coord(vert, horz);
                        cindex++;
                    }

                    //store tile in array
                    createdMap.MapArray[vert, horz] = tile;
                    //adjust indices
                    horz++;
                    if (horz == createdMap.MapArray.GetLength(1))
                    {
                        horz = 0;
                        vert++;
                    }

                }
                //assign the castle list to the map
                createdMap.CastleLocations = castleList;

                #endregion

                //TODO: code for reading in animated tiles and making a List of animatedTile classes
                //
                //
                //

                //now that the map is stored into an array, create a texture
                //for the overall map and one for each player (max 5 textures)
                Texture2D fullMap = new Texture2D(gd, createdMap.MapWidth, createdMap.MapHeight);
                createdMap.PlayerMaps = new Texture2D[createdMap.NumberOfPlayers];

                //creating the overall map
                #region Create Overall Map
                RenderTarget2D rendTarget = new RenderTarget2D(gd, createdMap.MapWidth, createdMap.MapHeight,
                                                false, gd.PresentationParameters.BackBufferFormat, DepthFormat.Depth24);
                //set the current render target to created target.
                gd.SetRenderTarget(rendTarget);
                //set the texture to transparent bg
                gd.Clear(Microsoft.Xna.Framework.Color.Transparent);
                //begin drawing
                sb.Begin();
                //get the length and width of array for for loop
                int verticalSize = createdMap.MapArray.GetLength(0);
                int horizontalSize = createdMap.MapArray.GetLength(1);
                //loop through all the elements of the array and convert to a texture
                for (int v = 0; v < verticalSize; v++)
                {
                    for (int h = 0; h < horizontalSize; h++)
                    {
                        //skip over indexes with borders
                        if (createdMap.MapArray[v, h] == -1) continue;
                        //draw the tile
                        sb.Draw(createdMap.TilesetImage, //draw from the tileset
                            new Microsoft.Xna.Framework.Rectangle((h * GameLibrary.TILESIZE),  //destination rectangle
                                (v * GameLibrary.TILESIZE), //draw a 32 x 32 tile into the appropriate space in the texture
                                GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                            new Microsoft.Xna.Framework.Rectangle((createdMap.MapArray[v, h] % GameLibrary.TILESIZE) * GameLibrary.TILESIZE, //source rectangle
                                (createdMap.MapArray[v, h] / GameLibrary.TILESIZE) * GameLibrary.TILESIZE,   //pulls the correct tile out of the tile set to draw to the screen
                                GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                            //color white to use unaltered texture
                            Microsoft.Xna.Framework.Color.White);
                    }
                }
                //end drawing phase
                sb.End();
                //reset the render target to original backbuffer
                gd.SetRenderTarget(null);
                //reset screen (might not need in this case)
                gd.Clear(Microsoft.Xna.Framework.Color.Black);
                //creat a color array to store the texcture data for each pixel
                Microsoft.Xna.Framework.Color[] cs = new Microsoft.Xna.Framework.Color[createdMap.MapWidth * createdMap.MapHeight];
                //pull out all of the data from the texture and store into array
                rendTarget.GetData<Microsoft.Xna.Framework.Color>(cs);
                //initialize the actual map image
                createdMap.MapImage = new Texture2D(gd, createdMap.MapWidth, createdMap.MapHeight);
                //store into a more permenant data texture, instead of a render target
                //map is now complete and ready to be drawn to the screen.
                createdMap.MapImage.SetData<Microsoft.Xna.Framework.Color>(cs);

                rendTarget.Dispose();
                #endregion

                //dispose of objects no longer needed
                fullMap.Dispose();

            }
            return createdMap;
        }

        //creates the individual player maps
        //can be called after a map is selected so
        //individual maps dont need to be generated when the
        //maps are loaded
        public static void generateIndividualMaps(GraphicsDevice gd, SpriteBatch sb, ref Map createdMap)
        {
            int PMWidth = 0, PMHeight = 0;
            createdMap.PlayerStartingArrays = new List<byte[,]>();
            //loop to get dimensions of the map for each player
            for (int i = 1; i <= createdMap.NumberOfPlayers; i++)
            {
                //get the starting coordinate for that player
                GameLogic.Coord start = checkBorders(i, createdMap.MapArray);
                //call flood fill for that player
                bool[,] playerInsideArray = floodFill(start.Horizontal, start.Vertical, createdMap.MapArray, i, ref PMWidth, ref PMHeight);
                //increment to account for 0 index
                PMWidth++; PMHeight++;

                //create the texture and render target
                Texture2D tempMap = new Texture2D(gd, PMWidth * GameLibrary.TILESIZE, PMHeight * GameLibrary.TILESIZE);
                RenderTarget2D tempTarget = new RenderTarget2D(gd, PMWidth * GameLibrary.TILESIZE, PMHeight * GameLibrary.TILESIZE);
                //create a byte array to store the starting positions of everything for the player
                byte[,] playerStart = new byte[PMHeight * 2, PMWidth * 2];
                //array indexes for playerStartArrays
                int psx = 0, psy = 0;

                //set render target to texture
                gd.SetRenderTarget(tempTarget);
                //set background to transparent
                gd.Clear(Microsoft.Xna.Framework.Color.Transparent);
                //start drawing
                sb.Begin();
                //loop through the section of the array for that player
                #region  Actual Drawing of the maps
                if (i == 1) //loop for player 1
                {
                    for (int y = start.Vertical; y < PMHeight; y++)
                    {
                        for (int x = start.Horizontal; x < PMWidth; x++)
                        {
                            if (playerInsideArray[y, x] == true)
                            {
                                sb.Draw(createdMap.TilesetImage, //draw from the tileset
                                        new Microsoft.Xna.Framework.Rectangle((x * GameLibrary.TILESIZE),  //destination rectangle
                                            (y * GameLibrary.TILESIZE), //draw a 32 x 32 tile into the appropriate space in the texture
                                             GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                        new Microsoft.Xna.Framework.Rectangle((createdMap.MapArray[y, x] % GameLibrary.TILESIZE) * GameLibrary.TILESIZE, //source rectangle
                                            (createdMap.MapArray[y, x] / GameLibrary.TILESIZE) * GameLibrary.TILESIZE,   //pulls the correct tile out of the tile set to draw to the screen
                                            GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                    //color white to use unaltered texture
                                        Microsoft.Xna.Framework.Color.White);
                                //if a castle is there, nothing can be built on top of it
                                if (createdMap.MapArray[y, x] == 0)
                                {
                                    playerStart[psy, psx] = 0;
                                    playerStart[psy + 1, psx] = 0;
                                    playerStart[psy, psx + 1] = 0;
                                    playerStart[psy + 1, psx + 1] = 0;
                                }
                                else //otherwise allow it to be built on
                                {
                                    playerStart[psy, psx] = 1;
                                    playerStart[psy + 1, psx] = 1;
                                    playerStart[psy, psx + 1] = 1;
                                    playerStart[psy + 1, psx + 1] = 1;
                                }
                            }
                            else //if the tile is outside the border, set the player
                            //start array in that area to be unbuildable
                            {
                                playerStart[psy, psx] = 0;
                                playerStart[psy + 1, psx] = 0;
                                playerStart[psy, psx + 1] = 0;
                                playerStart[psy + 1, psx + 1] = 0;
                            }
                            //increment x index
                            psx += 2;
                            if (psx >= playerStart.GetLength(1) - 1)
                            {
                                psx = 0;
                            }
                        }
                        //increment y index
                        psy += 2;
                        if (psy >= playerStart.GetLength(0) - 1)
                        {
                            psy = 0;
                        }
                    }
                }
                else if (i == 2)
                {
                    for (int y = start.Vertical; y < PMHeight; y++)
                    {
                        for (int x = start.Horizontal - PMWidth + 1; x < start.Horizontal + 1; x++)
                        {
                            if (playerInsideArray[y, x] == true)
                            {
                                sb.Draw(createdMap.TilesetImage, //draw from the tileset
                                        new Microsoft.Xna.Framework.Rectangle(((x - (start.Horizontal - PMWidth + 1)) * GameLibrary.TILESIZE),  //destination rectangle
                                            (y * GameLibrary.TILESIZE), //draw a 32 x 32 tile into the appropriate space in the texture
                                             GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                        new Microsoft.Xna.Framework.Rectangle((createdMap.MapArray[y, x] % GameLibrary.TILESIZE) * GameLibrary.TILESIZE, //source rectangle
                                            (createdMap.MapArray[y, x] / GameLibrary.TILESIZE) * GameLibrary.TILESIZE,   //pulls the correct tile out of the tile set to draw to the screen
                                            GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                    //color white to use unaltered texture
                                        Microsoft.Xna.Framework.Color.White);
                                //if a castle is there, nothing can be built on top of it
                                if (createdMap.MapArray[y, x] == 0)
                                {
                                    playerStart[psy, psx] = 0;
                                    playerStart[psy + 1, psx] = 0;
                                    playerStart[psy, psx + 1] = 0;
                                    playerStart[psy + 1, psx + 1] = 0;
                                }
                                else //otherwise allow it to be built on
                                {
                                    playerStart[psy, psx] = 1;
                                    playerStart[psy + 1, psx] = 1;
                                    playerStart[psy, psx + 1] = 1;
                                    playerStart[psy + 1, psx + 1] = 1;
                                }
                            }
                            else //if the tile is outside the border, set the player
                            //start array in that area to be unbuildable
                            {
                                playerStart[psy, psx] = 0;
                                playerStart[psy + 1, psx] = 0;
                                playerStart[psy, psx + 1] = 0;
                                playerStart[psy + 1, psx + 1] = 0;
                            }
                            //increment x index
                            psx += 2;
                            if (psx >= playerStart.GetLength(1) - 1)
                            {
                                psx = 0;
                            }
                        }
                        //increment y index
                        psy += 2;
                        if (psy >= playerStart.GetLength(0) - 1)
                        {
                            psy = 0;
                        }
                    }
                }
                else if (i == 3)
                {
                    for (int y = start.Vertical - PMHeight + 1; y < start.Vertical + 1; y++)
                    {
                        //this needs tested more, not sure if the minus 1 on length will work in all cases
                        //it wont, because border is in the corner, need to adjust.
                        for (int x = start.Horizontal - PMWidth + 1; x < start.Horizontal + 1; x++)
                        {
                            if (playerInsideArray[y, x] == true)
                            {
                                sb.Draw(createdMap.TilesetImage, //draw from the tileset
                                        new Microsoft.Xna.Framework.Rectangle(((x - (start.Horizontal - PMWidth + 1)) * GameLibrary.TILESIZE),  //destination rectangle
                                            ((y - (start.Vertical - PMHeight + 1)) * GameLibrary.TILESIZE), //draw a 32 x 32 tile into the appropriate space in the texture
                                             GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                        new Microsoft.Xna.Framework.Rectangle((createdMap.MapArray[y, x] % GameLibrary.TILESIZE) * GameLibrary.TILESIZE, //source rectangle
                                            (createdMap.MapArray[y, x] / GameLibrary.TILESIZE) * GameLibrary.TILESIZE,   //pulls the correct tile out of the tile set to draw to the screen
                                            GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                    //color white to use unaltered texture
                                        Microsoft.Xna.Framework.Color.White);
                                //if a castle is there, nothing can be built on top of it
                                if (createdMap.MapArray[y, x] == 0)
                                {
                                    playerStart[psy, psx] = 0;
                                    playerStart[psy + 1, psx] = 0;
                                    playerStart[psy, psx + 1] = 0;
                                    playerStart[psy + 1, psx + 1] = 0;
                                }
                                else //otherwise allow it to be built on
                                {
                                    playerStart[psy, psx] = 1;
                                    playerStart[psy + 1, psx] = 1;
                                    playerStart[psy, psx + 1] = 1;
                                    playerStart[psy + 1, psx + 1] = 1;
                                }
                            }
                            else //if the tile is outside the border, set the player
                                //start array in that area to be unbuildable
                            {
                                playerStart[psy, psx] = 0;
                                playerStart[psy + 1, psx] = 0;
                                playerStart[psy, psx + 1] = 0;
                                playerStart[psy + 1, psx + 1] = 0;
                            }
                            //increment x index
                            psx += 2;
                            if (psx >= playerStart.GetLength(1) - 1)
                            {
                                psx = 0;
                            }
                        }
                        //increment y index
                        psy += 2;
                        if (psy >= playerStart.GetLength(0) - 1)
                        {
                            psy = 0;
                        }
                    }
                }
                else
                {
                    for (int y = start.Vertical - PMHeight + 1; y < start.Vertical + 1; y++)
                    {
                        for (int x = start.Horizontal; x < PMWidth; x++)
                        {
                            if (playerInsideArray[y, x] == true)
                            {
                                sb.Draw(createdMap.TilesetImage, //draw from the tileset
                                        new Microsoft.Xna.Framework.Rectangle((x* GameLibrary.TILESIZE),  //destination rectangle
                                            ((y - (start.Vertical - PMHeight + 1)) * GameLibrary.TILESIZE), //draw a 32 x 32 tile into the appropriate space in the texture
                                             GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                        new Microsoft.Xna.Framework.Rectangle((createdMap.MapArray[y, x] % GameLibrary.TILESIZE) * GameLibrary.TILESIZE, //source rectangle
                                            (createdMap.MapArray[y, x] / GameLibrary.TILESIZE) * GameLibrary.TILESIZE,   //pulls the correct tile out of the tile set to draw to the screen
                                            GameLibrary.TILESIZE, GameLibrary.TILESIZE),
                                    //color white to use unaltered texture
                                        Microsoft.Xna.Framework.Color.White);
                                //if a castle is there, nothing can be built on top of it
                                if (createdMap.MapArray[y, x] == 0)
                                {
                                    playerStart[psy, psx] = 0;
                                    playerStart[psy + 1, psx] = 0;
                                    playerStart[psy, psx + 1] = 0;
                                    playerStart[psy + 1, psx + 1] = 0;
                                }
                                else //otherwise allow it to be built on
                                {
                                    playerStart[psy, psx] = 1;
                                    playerStart[psy + 1, psx] = 1;
                                    playerStart[psy, psx + 1] = 1;
                                    playerStart[psy + 1, psx + 1] = 1;
                                }
                            }
                            else //if the tile is outside the border, set the player
                                //start array in that area to be unbuildable
                            {
                                playerStart[psy, psx] = 0;
                                playerStart[psy + 1, psx] = 0;
                                playerStart[psy, psx + 1] = 0;
                                playerStart[psy + 1, psx + 1] = 0;
                            }
                            //increment x index
                            psx += 2;
                            if (psx >= playerStart.GetLength(1) - 1)
                            {
                                psx = 0;
                            }
                        }
                        //increment y index
                        psy += 2;
                        if (psy >= playerStart.GetLength(0) - 1)
                        {
                            psy = 0;
                        }
                    }
                }
                createdMap.PlayerStartingArrays.Add(playerStart);
                #endregion

                //end drawing
                sb.End();
                //revert render target
                gd.SetRenderTarget(null);
                //reset screen (might not need in this case)
                gd.Clear(Microsoft.Xna.Framework.Color.Black);
                //creat a color array to store the texcture data for each pixel
                Microsoft.Xna.Framework.Color[] cs = new Microsoft.Xna.Framework.Color[tempMap.Width * tempMap.Height];
                //pull out all of the data from the texture and store into array
                tempTarget.GetData<Microsoft.Xna.Framework.Color>(cs);
                //initialize the actual map image for the player
                createdMap.PlayerMaps[i - 1] = new Texture2D(gd, tempMap.Width, tempMap.Height);
                //store into a more permenant data texture, instead of a render target
                //map is now complete and ready to be drawn to the screen.
                createdMap.PlayerMaps[i - 1].SetData<Microsoft.Xna.Framework.Color>(cs);

            }
        }

        //pass (by reference to alter string) the map data read from file.
        //extract sections of the string to fill in required data.
        private static String interpData(ref string mapData)
        {
            string tempData = mapData.Substring(0, mapData.IndexOf('\r'));
            int k = mapData.IndexOf('\n');
            mapData = mapData.Substring(k + 1, mapData.LastIndexOf('\0') - k);
            return tempData;
        }

        //function to extract a single character from the map array in the map file
        private static int readCharacterAsInt(ref string mapData)
        {
            //get indexes of next space and next new line
            int k = mapData.IndexOf(' ');
            int r = mapData.IndexOf('\r');
            //if neither a space or newline is found, the end of map file is reached, return
            if (r == -1 && k == -1)
            {
                return -2;
            }
            //holds next tile pulled from file
            string tempData;
            //if a newline is found before a space, adjust indexes to remove the newline and get next tile
            if (r < k || k == -1)
            {
                tempData = mapData.Substring(0, r);
                mapData = mapData.Substring(r + 2, mapData.LastIndexOf('\0') - (r + 2));
            }
            else //otherwise skip the space and get next tile
            {
                tempData = mapData.Substring(0, k);
                mapData = mapData.Substring(k + 1, mapData.LastIndexOf('\0') - k);
            }
            //if the data is a B, a border is found, represented as -1
            if (tempData == "B")
            {
                return -1;
            }
            //if the data is a C, a castle is found, represented as 0
            if (tempData == "C")
            {
                return 0;
            }
            //otherwise return the integer from the map
            return Convert.ToInt32(tempData);
        }

        //checks for the start of a player area, in case the corners are a border
        private static GameLogic.Coord checkBorders(int playerNum, int[,] mapArray)
        {
            GameLogic.Coord returnCoord = new GameLogic.Coord();
            switch (playerNum)
            {
                case 1:
                    {
                        //start in top left corner and work right to find start for P1
                        for (int y = 0; y < mapArray.GetLength(0); y++)
                        {
                            for (int x = 0; x < mapArray.GetLength(1); x++)
                            {
                                if (mapArray[y, x] != -1)
                                {
                                    returnCoord.Vertical = y;
                                    returnCoord.Horizontal = x;
                                    return returnCoord;
                                }
                            }
                        }
                        break;
                    }
                case 2:
                    {
                        //start in top right corner and work down to find start for P2
                        for (int x = mapArray.GetLength(1) - 1; x >= 0; x--)
                        {
                            for (int y = 0; y < mapArray.GetLength(0); y++)
                            {
                                if (mapArray[y, x] != -1)
                                {
                                    returnCoord.Vertical = y;
                                    returnCoord.Horizontal = x;
                                    return returnCoord;
                                }
                            }
                        }
                        break;
                    }
                case 3:
                    {
                        //start in bottom right corner and work left to find start for P3
                        for (int y = mapArray.GetLength(0) - 1; y >= 0; y--)
                        {
                            for (int x = mapArray.GetLength(1) - 1; x >= 0; x--)
                            {
                                if (mapArray[y, x] != -1)
                                {
                                    returnCoord.Vertical = y;
                                    returnCoord.Horizontal = x;
                                    return returnCoord;
                                }
                            }
                        }
                        break;
                    }
                case 4:
                    {
                        //start in bottom left corner and work up to find start for P4
                        for (int x = 0; x < mapArray.GetLength(1); x++)
                        {
                            for (int y = mapArray.GetLength(0) - 1; y >= 0; y--)
                            {
                                if (mapArray[y, x] != -1)
                                {
                                    returnCoord.Vertical = y;
                                    returnCoord.Horizontal = x;
                                    return returnCoord;
                                }
                            }
                        }
                        break;
                    }
                default:
                    {
                        //error, bad map (all borders maybe)
                        break;
                    }
            }
            return returnCoord;
        }

        //performs a simple flood fill to get the max width and height of a sub map
        //it also generates a boolean array to determine which cells are inside the player's
        //boundaries and returns it so the gernerateIndividualMaps function can create the texture
        private static bool[,] floodFill(int startX, int startY, int[,] mapArray, int playerNum, ref int width, ref int height)
        {
            #region Create Visited Matrix
            //create visited matrix from mapArray
            int horzSize, vertSize;
            //array dimensions
            horzSize = mapArray.GetLength(1);
            vertSize = mapArray.GetLength(0);
            //visit array
            bool[,] visitArray = new bool[vertSize, horzSize];
            //convert map data to visit data
            for (int y = 0; y < vertSize; y++)
            {
                for (int x = 0; x < horzSize; x++)
                {
                    if (mapArray[y, x] > -1)
                    { //not yet visited
                        visitArray[y, x] = false;
                    }
                    else
                    { //already visited (border)
                        visitArray[y, x] = true;
                    }
                }
            }
            #endregion
            //create a matrix that stores a 1 if the tile is inside the boundaries
            //for that player, a 0 if it is not, then return the matrix to draw
            #region create Inside Matrix

            bool[,] InsideArray = new bool[vertSize, horzSize];
            //create blank array to be filled in
            for (int y = 0; y < vertSize; y++)
            {
                for (int x = 0; x < horzSize; x++)
                {
                    //init all to false
                    InsideArray[y, x] = false;
                }
            }


            #endregion

            //now that a visit matrix is created, perform flood fill
            Queue<GameLogic.Coord> nodeQueue = new Queue<GameLogic.Coord>();
            width = 0;
            height = 0;
            //put the first coordinate into the queue
            nodeQueue.Enqueue(new GameLogic.Coord(startY, startX));
            //while elements are still in the queue
            while (nodeQueue.Count > 0)
            {
                //remove the first element in the queue
                GameLogic.Coord c = nodeQueue.Dequeue();
                //if the node has yet to be visited
                if (visitArray[c.Vertical, c.Horizontal] == false)
                {
                    //if else to determine player
                    //check to see if the current coordinate
                    //is further out then the current width
                    //or height, and if it is set the new
                    //width or height value.
                    if (playerNum == 1)
                    {
                        if (c.Vertical > height)
                        {
                            height = c.Vertical;
                        }
                        if (c.Horizontal > width)
                        {
                            width = c.Horizontal;
                        }
                    }
                    else if (playerNum == 2)
                    {
                        if (c.Vertical > height)
                        {
                            height = c.Vertical;
                        }
                        if (startX - c.Horizontal > width)
                        {
                            width = startX - c.Horizontal;
                        }
                    }
                    else if (playerNum == 3)
                    {
                        if (startY - c.Vertical > height)
                        {
                            height = startY - c.Vertical;
                        }
                        if (startX - c.Horizontal > width)
                        {
                            width = startX - c.Horizontal;
                        }
                    }
                    else
                    {
                        if (startY - c.Vertical > height)
                        {
                            height = startY - c.Vertical;
                        }
                        if (c.Horizontal > width)
                        {
                            width = c.Horizontal;
                        }
                    }
                    //set this entry to visited (so the lop doesnt check it again
                    //as well as setting coordinates to true since it is inside the
                    //players borders.
                    visitArray[c.Vertical, c.Horizontal] = true;
                    InsideArray[c.Vertical, c.Horizontal] = true;
                    //check all the coordinates around the current one, making sure that
                    //the indexes do not go out of bounds.  Add valid coordinates into queue.
                    if (c.Horizontal - 1 >= 0)
                    {
                        nodeQueue.Enqueue(new GameLogic.Coord(c.Vertical, c.Horizontal - 1));
                    }
                    if (c.Horizontal + 1 < visitArray.GetLength(1))
                    {
                        nodeQueue.Enqueue(new GameLogic.Coord(c.Vertical, c.Horizontal + 1));
                    }
                    if (c.Vertical - 1 >= 0)
                    {
                        nodeQueue.Enqueue(new GameLogic.Coord(c.Vertical - 1, c.Horizontal));
                    }
                    if (c.Vertical + 1 < visitArray.GetLength(0))
                    {
                        nodeQueue.Enqueue(new GameLogic.Coord(c.Vertical + 1, c.Horizontal));
                    }
                }
            }
            //return the insideArray so the map can be generated easily.          
            return InsideArray;
        }
    }
}
