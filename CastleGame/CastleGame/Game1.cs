using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using CastleGame.GameLogic;
using CastleGame.Graphics;
using CastleGame.Input;

namespace CastleGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        InputHelper inputData;

        //variables
        Map[] mapList;

        //Game State Class Vars
        MapSelection mapSelection;
        

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
//set window properties
#if WINDOWS
            graphics.IsFullScreen = false;
#else
            graphics.IsFullScreen = true;
#endif
            //set screen size
            graphics.PreferMultiSampling = true;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;

            //set to a default 30 fps
            TargetElapsedTime = TimeSpan.FromTicks(333333);
            IsFixedTimeStep = false;
            //show the Mouse on windows platform
#if WINDOWS
            IsMouseVisible = true;
#endif
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // set initial states
            //GameLibrary.currentGameState = GameState.Menu;
            //GameLibrary.currentMenuState = MenuState.Title;

            //init input
            inputData = new InputHelper();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //load the game font - used throughout the program
            GameLibrary.GameFont = Content.Load<SpriteFont>(@"Text/GameFont");

            //initialize all map data in the directory
            //provide loading bar if needed
            initMaps();
            //create the Map selection var
            mapSelection = new MapSelection(mapList);
            
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (GameLibrary.currentGameState == GameState.Menu && GameLibrary.currentMenuState == MenuState.MapSelect)
            {
                mapSelection.updateMapSelection(GraphicsDevice, spriteBatch, gameTime, inputData);
            }
            

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            if (GameLibrary.currentGameState == GameState.Menu && GameLibrary.currentMenuState == MenuState.MapSelect)
            {
                mapSelection.drawMapSelection(spriteBatch);
            }


            if (GameLibrary.currentGameState == GameState.AttackPhase ||
                GameLibrary.currentGameState == GameState.BuyPhase ||
                GameLibrary.currentGameState == GameState.RebuildPhase)
            {
                if (mapSelection.CurrentMap != null)
                {
                    //testing map drawing
                    spriteBatch.Begin();

                    //spriteBatch.Draw(currentMap.MapImage, new Rectangle(0, 0, currentMap.MapWidth, currentMap.MapHeight), Color.White);

                    spriteBatch.Draw(mapSelection.CurrentMap.PlayerMaps[1], 
                        new Rectangle(400, 0, mapSelection.CurrentMap.PlayerMaps[1].Width, mapSelection.CurrentMap.PlayerMaps[1].Height), Color.White);

                    spriteBatch.End();
                    //end test
                }
            }

            base.Draw(gameTime);
        }

        //region for all init functions
        #region Initialize Functions
        //initializes all the map data in the maps directory
        private void initMaps()
        {
            //get the maps in the Maps folder and allocate memory for the maps
            Graphics.MapDecoder.getMapsInDirectory(Content.RootDirectory);
            mapList = new Map[Graphics.MapDecoder.mapFiles.Count];
            //gather data from all the map in the directory
            for (int index = 0; index < Graphics.MapDecoder.mapFiles.Count; index++)
            {
                mapList[index] = Graphics.MapDecoder.generateMap(Graphics.MapDecoder.mapFiles[index].FullName, Content.RootDirectory, Content, GraphicsDevice, spriteBatch);
                //currentMap = Graphics.MapDecoder.generateMap(Graphics.MapDecoder.mapFiles[1].FullName, Content.RootDirectory, Content, GraphicsDevice, spriteBatch);   
            }
            //Graphics.MapDecoder.generateIndividualMaps(GraphicsDevice, spriteBatch, ref currentMap);
        }

        #endregion
    }
}
